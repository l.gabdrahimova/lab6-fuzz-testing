import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");

const STANDARD = { label: 'Standard', factor: 0.05 }
const PREMIUM = { label: 'Premium', factor: 0.1 }
const DIAMOND = { label: 'Diamond', factor: 0.2 }
const NONSENSE = { label: 'Nonsense', factor: 0 }

const TEST_VALUES = [
    {factor: 1, value: 5000},
    {factor: 1.5, value: 10000},
    {factor: 1.5, value: 25000},
    {factor: 2, value: 50000},
    {factor: 2, value: 75000},
    {factor: 2.5, value: 100000},
    {factor: 2.5, value: 200000}
    ]

describe('tests standard', () => {
    const m = 0.05
    const p = 'Standard'
    test('test 1',  (done) => {
        assert.equal(calculateBonuses(p, 5000), m);
        done();
    });
    test('test 2',  (done) => {
        assert.equal(calculateBonuses(p, 10000), m * 1.5);
        done();
    });
    test('test 3',  (done) => {
        assert.equal(calculateBonuses(p, 25000), m * 1.5);
        done();
    });
    test('test 4',  (done) => {
        assert.equal(calculateBonuses(p, 50000), m * 2);
        done();
    });
    test('test 5',  (done) => {
        assert.equal(calculateBonuses(p, 75000), m * 2);
        done();
    });
    test('test 6',  (done) => {
        assert.equal(calculateBonuses(p, 100000), m * 2.5);
        done();
    });
    test('test 7',  (done) => {
        assert.equal(calculateBonuses(p, 200000), m * 2.5);
        done();
    });
});


describe('tests premium', () => {
    const m = 0.1
    const p = 'Premium'
    test('test 1',  (done) => {
        assert.equal(calculateBonuses(p, 5000), m);
        done();
    });
    test('test 2',  (done) => {
        assert.equal(calculateBonuses(p, 10000), m * 1.5);
        done();
    });
    test('test 3',  (done) => {
        assert.equal(calculateBonuses(p, 25000), m * 1.5);
        done();
    });
    test('test 4',  (done) => {
        assert.equal(calculateBonuses(p, 50000), m * 2);
        done();
    });
    test('test 5',  (done) => {
        assert.equal(calculateBonuses(p, 75000), m * 2);
        done();
    });
    test('test 6',  (done) => {
        assert.equal(calculateBonuses(p, 100000), m * 2.5);
        done();
    });
    test('test 7',  (done) => {
        assert.equal(calculateBonuses(p, 200000), m * 2.5);
        done();
    });
});


describe('tests diamond', () => {
    const m = 0.2
    const p = 'Diamond'
    test('test 1',  (done) => {
        assert.equal(calculateBonuses(p, 5000), m);
        done();
    });
    test('test 2',  (done) => {
        assert.equal(calculateBonuses(p, 10000), m * 1.5);
        done();
    });
    test('test 3',  (done) => {
        assert.equal(calculateBonuses(p, 25000), m * 1.5);
        done();
    });
    test('test 4',  (done) => {
        assert.equal(calculateBonuses(p, 50000), m * 2);
        done();
    });
    test('test 5',  (done) => {
        assert.equal(calculateBonuses(p, 75000), m * 2);
        done();
    });
    test('test 6',  (done) => {
        assert.equal(calculateBonuses(p, 100000), m * 2.5);
        done();
    });
    test('test 7',  (done) => {
        assert.equal(calculateBonuses(p, 200000), m * 2.5);
        done();
    });
});


describe('tests wrong input', () => {
    const p = 'wrong'
    test('test 1',  (done) => {
        assert.equal(calculateBonuses(p, 5000), 0);
        done();
    });
    test('test 2',  (done) => {
        assert.equal(calculateBonuses(p, 10000), 0);
        done();
    });
    test('test 3',  (done) => {
        assert.equal(calculateBonuses(p, 25000), 0);
        done();
    });
    test('test 4',  (done) => {
        assert.equal(calculateBonuses(p, 50000), 0);
        done();
    });
    test('test 5',  (done) => {
        assert.equal(calculateBonuses(p, 75000), 0);
        done();
    });
    test('test 6',  (done) => {
        assert.equal(calculateBonuses(p, 100000), 0);
        done();
    });
    test('test 7',  (done) => {
        assert.equal(calculateBonuses(p, 200000), 0);
        done();
    });
});
